############################################################
DyPy - A collection of python tools for atmospheric sciences
############################################################

A recent build of the documentation is available at https://dypy.readthedocs.io/en/latest/ |docs|

.. |docs| image:: https://readthedocs.org/projects/dypy/badge/?version=latest
   :target: http://dypy.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status


