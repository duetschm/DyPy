# coding: utf-8
"""dy

Helpful function for python
"""
import sys


def merge_dict(dict_1, dict_2):
    """Merge two dictionaries.
    Values that evaluate to true take priority over falsy values.
    `dict_1` takes priority over `dict_2`.
    """
    return dict((str(key), dict_1.get(key) or dict_2.get(key))
                for key in set(dict_2) | set(dict_1))


def ipython(__globals__, __locals__, __msg__=None, __err__=66):
    """Drop into an iPython shell with all global and local variables.

    To pass the global and local variables, call with globals() and locals()
    as arguments, respectively. Notice how both functions must called in the
    call to ipython.

    To exit the program after leaving the iPython shell, pass an integer,
    which will be returned as the error code.

    To display a message when dropping into the iPython shell, pass a
    string after the error code.

    Examples
    --------
    >>> ipython(globals(), locals(), "I'm here!")

    """
    import warnings
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        warnings.filterwarnings("ignore", category=PendingDeprecationWarning)
        import IPython
        print('\n----------\n')
        globals().update(__globals__)
        locals().update(__locals__)
        if __msg__ is not None:
            print("\n{l}\n{m}\n{l}\n".format(l="*" * 60, m=__msg__))
        IPython.embed(display_banner=None)
        if __err__ is not None:
            exit(__err__)


def print_args(args, nt=40):
    """Print input arguments obtained by argparse.

    Example:
    --------
    >>> parser = argparse.ArgumentParser(...)
        ...
    >>> args = parser.parse_args()
    >>> print_args(args)

    """
    if nt > 0:
        print("=" * nt)
    nn = max([len(k) for k in vars(args)])
    for name, arg in sorted(vars(args).items()):
        if isinstance(arg, (list, tuple)):
            arg = ", ".join([str(a) for a in arg])
        print("{{:{}}} : {{}}".format(nn).format(name.upper(), arg))
    if nt > 0:
        print("=" * nt)
