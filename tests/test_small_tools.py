import os
import numpy as np
import numpy.ma as ma
from numpy.testing import assert_almost_equal

from dypy.small_tools import (read_shapefile, mask_polygon,
                              potential_temperature, CrossSection,
                              great_circle_distance, rotate_points)

testdatadir = os.path.join(os.path.dirname(__file__), 'test_data')


def test_read_shapefile_single_polygon():
    shapefile = os.path.join(testdatadir, 'shapefile_single_polygon.shp')
    pnts, projections = read_shapefile(shapefile)
    test_pnts = np.array([[8.61686488, 46.08349113],
                          [8.47050174, 45.98639637],
                          [8.34168295, 46.22408627],
                          [8.48540242, 46.29762834],
                          [8.61686488, 46.08349113]])
    assert_almost_equal(pnts[0], test_pnts)
    assert projections == "+init=epsg:4326"


def test_mask_polygon():

    array = np.zeros((20, 20)) + 10.
    polygon = np.array([[5, 5],
                        [10, 5],
                        [10, 10],
                        [5, 10],
                        [5, 5]])
    lon = np.arange(0, 20, dtype=float)
    lat = np.arange(0, 20, dtype=float)
    lon, lat = np.meshgrid(lon, lat)

    clipped = mask_polygon(polygon, array, lon, lat)

    test_clip = np.zeros((20, 20))
    test_clip[5:10, 5:10] = 10
    test_clip = ma.masked_not_equal(test_clip, 10)

    assert_almost_equal(clipped, test_clip)


def test_potential_temperature():

    t = 15. + 273.15  # K
    p = 750.  # hPa
    th = potential_temperature(t, p)
    assert_almost_equal(th, 312.83, decimal=1)


def test_crosssection():
    rlon = np.arange(0, 20, 1)
    rlat = np.arange(0, 20, 1)
    qv = np.zeros((10, 20, 20))
    qv[:, :, 9:11] = 10
    coo = [(24, 46.07452973), (31, 64.3)]
    variables = {'rlon': rlon,
                 'rlat': rlat,
                 'qv': qv}
    cross = CrossSection(variables, coo, np.arange(0, 10))
    distance = great_circle_distance(coo[0][0], coo[0][1],
                                     coo[1][0], coo[1][1])
    assert cross.distance == distance
    np.testing.assert_allclose(cross.qv, np.ones_like(cross.qv) * 10)


def test_crosssection_regular():
    lon = np.arange(10, 21, 1)
    lat = np.arange(40, 51, 1)
    qv = np.zeros((10, 11, 11))
    qv[:, :, 3:6] = 10
    coo = [(14, 40), (14, 50)]
    variables = {'lon': lon,
                 'lat': lat,
                 'qv': qv}
    cross = CrossSection(variables, coo, np.arange(0, 10), version='regular')
    np.testing.assert_allclose(cross.qv, np.ones_like(cross.qv) * 10)


def test_great_circle():
    assert great_circle_distance(0, 55, 8, 45.5) == 1199.3240879770135


def test_rotate_points():
    rlon, rlat = rotate_points(-170, 43, 8, 47)
    np.testing.assert_allclose(rlon, np.array([-1.36384859]))
    np.testing.assert_allclose(rlat, np.array([0.01740901]))
    lon, lat = rotate_points(-170, 43, 0, 0, direction='r2n')
    np.testing.assert_allclose(lon, np.array([10.]))
    np.testing.assert_allclose(lat, np.array([47.0]))
