Lagranto
--------

The goal of this section is show how to calculate trajectories, analyzed them and plot them using the :ref:`lagranto-package`.

Calculation
^^^^^^^^^^^

The :ref:`lagranto-package` provide a class, :class:`dypy.lagranto.LagrantoRun`, to wrap the Lagranto programs in python. It allow to calculate trajectories in parallel. You can take a look at the docstring to get familiar with the class.

Let's say that we want to calculate Warm Conveyor Belt trajectories for a 5 day period in June 2013.
Using Era-Interim we can start trajectories every 6 hour and we will calculate them for 48 hours forward in time. Since :class:`dypy.lagranto.LagrantoRun` needs a list of (startdate, enddate), we can build the dates as follow:

.. code-block:: python

 from datetime import datetime, timedelta
 from dypy.small_tools import interval
 startdate = datetime(2013, 6, 1, 0)
 enddate = startdate + timedelta(days=5)
 dates = [(d, d + timedelta(hours=48))
 for d in interval(startdate, enddate,
                   timedelta(hours=6))]


If the Era-interim data are in the `erainterim` folder and if the output files should be written in the `output` folder, then the :class:`dypy.lagranto.LagrantoRun` can be initialized as follow:

.. code-block:: python

 from dypy.lagranto import LagrantoRun
 lrun = LagrantoRun(dates, workingdir='erainterim',
                    outputdir='output', version='ecmwf')


We want to start the trajectories every 20km in the box [5E, 40E, 30N, 60N], so let's create a starting file:

.. code-block:: python

 specifier = "'box.eqd(5,20,40,50,20)@profile(850,500,10)@hPa'"
 out_create_startf = lrun.create_startf(startdate, specifier, tolist=True)

The `tolist` argument is needed if we want to use the same staring file for all starting time of the trajectories.
We can now calculate the trajectories, but first starting for a single date to test our setup:

.. code-block:: python

 out_caltra = lrun.caltra(*dates[1])

We can also test tracing Q along a trajectories:

.. code-block:: python

 out_trace = lrun.trace(dates[1][0], field='Q 1.')

We can now calculate and trace the trajectories in parallel, but for this we will use a tracevars file:

.. code-block:: python

 tracevars = """Q         1.    0 P
 U          1.    0 P
 """
 out = lrun.run_parallel(trace_kw={'tracevars_content': tracevars}, type='both')

The `tracevars_content` keyword argument will be passed to trace to create a tracevars file with Q and U.
The `type` keyword argument determine what is run in parallel, currently both, trace, and caltra are available.


Analyze
^^^^^^^
*Next steps to be done*
